public class Card {
    private String suit;
    private String value;

    public Card(String suit, String value){
            this.suit = suit;
            this.value = value;	
    }
    public String getsuit(){return this.suit;}		
    public String getvalue(){return this.value;}
    public String toString(){
        return this.value + " of " + this.suit; 
    }
		public double calculateScore(){
						int units = Integer.parseInt(this.value);
						if (this.suit.equals("Spades")){
										return units + 0.1; 
						} else if (this.suit.equals("Hearts")){
										return units + 0.4;
						} else if (this.suit.equals("Diamonds")){
										return units + 0.3;
						} else {
										return units + 0.2;
						}
														

		}

}
